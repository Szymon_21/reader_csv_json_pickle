import json
import sys
from modules.CSV import File_reader_csv
from modules.JSON import File_reader_json
from modules.PICKLE import File_reader_pickle

print(30 * '#')
print('FILE EDITOR')
print(30 * '#')

def Validate(argv):
    if len(argv) < 5:
        raise Exception("Podano za mało parametrów.")
    elif len(argv) > 7:
        raise Exception("Podano za dużo parametrów.")
     
    try:
        int(argv[3])
    except ValueError:
        raise ValueError("Wartość nie jest liczbą.")


def Parser(argv):
    Validate(argv)

    if len(argv) != 6:
        return (argv[1], argv[2], argv[3], argv[4], None) 
    return (argv[1], argv[2], argv[3], argv[4], argv[5])
  

def main():
    src, dst, column, value_X, text_to_change = Parser(sys.argv)

    if src.split(".")[1] == "csv" and dst.split(".")[1] == "csv":
        csv_reader =   File_reader_csv(src, dst, column, value_X, text_to_change)
        csv_reader.Process()

    elif src.split(".")[1] == "json" and dst.split(".")[1] == "json":
        json_reader =  File_reader_json(src, dst, column, value_X, text_to_change)
        json_reader.Process()

    elif src.split(".")[1] == "pkl" and dst.split(".")[1] == "pkl":
        pickle_reader =  File_reader_pickle(src, dst, column, value_X, text_to_change)
        pickle_reader.Process()
    
if __name__ == '__main__':
    main()