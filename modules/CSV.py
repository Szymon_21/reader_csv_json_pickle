import pprint
import pprint
import csv
import pandas as pd

class File_reader_csv:
    def __init__(self, src, dst, column, value_X, text_to_change):

        self.src = src
        self.dst = dst
        self.column = int(column)
        self.value_X = int(value_X)
        self.text_to_change = text_to_change

        self.input_data = []
        self.output_data = []
  
    def Read_and_Edit_file(self):
        file = pd.read_csv(self.src)
        pprint.pprint(file)

        df =file.iloc[self.value_X, self.column]
        print(df)
        
        df_change = file.replace([df], self.text_to_change)
        print(df_change)
        self.output_data.append(df_change)
        print(self.output_data)
        
    def Save_to_file(self):
        with open(self.dst, "w") as file_to_save:
            writer = csv.writer(file_to_save)        
            for row in self.output_data:
                writer.writerow(row)

    def Process(self):
        self.Read_and_Edit_file()
        self.Save_to_file()