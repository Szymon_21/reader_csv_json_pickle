import pprint
import csv
import os
import pandas as pd

class File_reader_json:
    def __init__(self, src, dst, column, value_X, text_to_change):

        self.src = src
        self.dst = dst
        self.column = int(column)
        self.value_X = int(value_X)
        self.text_to_change = text_to_change

        self.input_data = []
        self.output_data = []

    def open_file(self):
        dfo = pd.read_json(self.src, orient='index')
        self.csvData = dfo.to_csv(index=False)

    def save_data(self):
        dfs = pd.read_csv (r'for_a_while_file_json.csv' )
        self.jsonData = dfs.to_json (self.dst)

    def Read_and_Edit_file(self):
        self.open_file()

        file = pd.read_csv(self.csvData)
        pprint.pprint(file)

        df =file.iloc[self.value_X, self.column]
        print(df)
        
        df_change = file.replace([df], self.text_to_change)
        print(df_change)
        self.output_data.append(df_change)
        print(self.output_data)
        
    def Save_to_file(self):
        with open('for_a_while_file_json.csv', "w") as file_to_save:
            writer = csv.writer(file_to_save)        
            for row in self.output_data:
                writer.writerow(row)

        self.save_data()
        os.remove('for_a_while_file_json.csv')

    def Process(self):
        self.Read_and_Edit_file()
        self.Save_to_file()